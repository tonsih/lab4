package lab4.data;

import java.util.Observable;

/**
 * Represents the 2-d game grid
 * @author Toni Sihvola
 * @author Mojtaba Rezaei
 */

public class GameGrid extends Observable {
	public static final int EMPTY = 0;
	public static final int ME = 1;
	public static final int OTHER = 2;

	private final int INROW = 5;

	private int[][] boardArray;

	/**
	 * Constructor
	 * 
	 * @param size The width/height of the game grid
	 */
	public GameGrid(int size) {
		boardArray = new int[size][size];

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				boardArray[i][j] = EMPTY;
			}
		}

	}

	/**
	 * Reads a location of the grid
	 * 
	 * @param x The x coordinate
	 * @param y The y coordinate
	 * @return the value of the specified location
	 */

	public int getLocation(int x, int y) {
		return boardArray[x][y];
	}

	/**
	 * Returns the size of the grid
	 * 
	 * @return the grid size
	 */
	public int getSize() {
		return boardArray.length;
	}

	/**
	 * Enters a move in the game grid
	 * 
	 * @param x      the x position
	 * @param y      the y position
	 * @param player
	 * @return true if the insertion worked, false otherwise
	 */

	public boolean move(int x, int y, int player) {
		if (boardArray[x][y] == EMPTY) {
			boardArray[x][y] = player;
			setChanged();
			notifyObservers();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Clears the grid of pieces
	 */

	public void clearGrid() {
		for (int i = 0; i < this.getSize(); i++) {
			for (int j = 0; j < this.getSize(); j++) {
				boardArray[i][j] = EMPTY;
			}
		}

		setChanged();
		notifyObservers();
	}


	/**
	 * Checks for win in vertical direction.
	 * 
	 * @param player
	 * @param x_1    the starting x position in the array
	 * @param x_2    one bigger than the ending x position in the array
	 * @param y_1    the starting y position in the array
	 * @param y_1    one bigger than the ending y position in the array
	 * @return true if win in vertical direction, otherwise false
	 */
	private boolean checkWin(int player, int x_1, int x_2, int y_1, int y_2, 
			int dir) {
		for (int x = x_1; x < x_2; x++) {
			for (int y = y_1; y < y_2; y++) {
				kloop: for (int k = 0; k < INROW; k++) {

					switch (dir) {

					case 0:
						if (boardArray[x][y + k] != player) {
							break kloop;
						};

						break;

					case 1:
						if (boardArray[x + k][y] != player) {
							break kloop;
						};

						break;

					case 2:
						if (boardArray[x + k][y + k] != player) {
							break kloop;
						};

						break;

					case 3:
						if (boardArray[x - k][y + k] != player) {
							break kloop;
						};

						break;

					}

					if (k == INROW - 1) {
						return true;
					}

				}

			}

		}
		return false;

	}

	/**
	 * Check if a player has won
	 * 
	 * @param player the player to check for
	 * @return true if player has won, false otherwise
	 */
	public boolean isWinner(int player) {

		if (this.getSize() >= INROW) {
			if (
	checkWin(player, 0, this.getSize(), 0, (this.getSize() - INROW) + 1, 0) || 
	
	checkWin(player, 0, (this.getSize() - INROW) + 1, 0, this.getSize(), 1) || 
	
	checkWin(player, 0, (this.getSize() - INROW) + 1, 0, 
			(this.getSize() - INROW) + 1, 2) 								|| 
	
	checkWin(player, INROW - 1, this.getSize(), 0, 
			(this.getSize() - INROW) + 1, 3)) {
				return true;
			}

		}

		return false;

	}

}
