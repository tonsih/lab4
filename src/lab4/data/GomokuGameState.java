package lab4.data;

import java.util.Observable;
import java.util.Observer;

import lab4.client.GomokuClient;

/**
 * Represents the state of a game
 * @author Toni Sihvola
 * @author Mojtaba Rezaei
 */

public class GomokuGameState extends Observable implements Observer{

   // Game variables
	private final int DEFAULT_SIZE = 15;
	private GameGrid gameGrid;
	
    //Possible game states
	private final int NOT_STARTED = 0;
	private final int MY_TURN = 1;
	private final int OTHER_TURN = 2;
	private final int FINISHED = 3; 

	private int currentState;
	
	private GomokuClient client;
	
	private String message;
	
	/**
	 * The constructor
	 * 
	 * @param gc The client used to communicate with the other player
	 */
	public GomokuGameState(GomokuClient gc){
		client = gc;
		client.addObserver(this);
		gc.setGameState(this);
		currentState = NOT_STARTED;
		gameGrid = new GameGrid(DEFAULT_SIZE);
	}
	

	/**
	 * Returns the message string
	 * 
	 * @return the message string
	 */
	public String getMessageString(){
		return message;
	}
	
	/**
	 * Returns the game grid
	 * 
	 * @return the game grid
	 */
	public GameGrid getGameGrid(){
		return gameGrid;
	}

	/**
	 * This player makes a move at a specified location
	 * 
	 * @param x the x coordinate
	 * @param y the y coordinate
	 */
	
	// add more conditions for eg. currentState == OTHER_TURN etc...
	public void move(int x, int y){
		
		if (currentState == MY_TURN) {
			if (gameGrid.getLocation(x, y) == gameGrid.EMPTY) {
				gameGrid.move(x, y, gameGrid.ME);
				currentState = OTHER_TURN;
				message = "Wait for your turn";
				client.sendMoveMessage(x, y);
				
			} else {
				message = "This square is taken!";
			}
			
		}
		
		
		if (gameGrid.isWinner(gameGrid.ME)) {
			currentState = FINISHED;
			message = "You win!";
		}
		
				
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Starts a new game with the current client
	 */
	public void newGame(){
		gameGrid.clearGrid();
		currentState = OTHER_TURN;
		message = "Game started, waiting for other player...";
		client.sendNewGameMessage();
		setChanged();
		notifyObservers();
		
	}
	
	/**
	 * Other player has requested a new game, so the 
	 * game state is changed accordingly
	 */
	public void receivedNewGame(){
		gameGrid.clearGrid();
		currentState = MY_TURN;
		message = "Game started, waiting for...";
		
		setChanged();
		notifyObservers();
		
	}
	
	/**
	 * The connection to the other player is lost, 
	 * so the game is interrupted
	 */
	public void otherGuyLeft(){
		gameGrid.clearGrid();
		currentState = FINISHED;
		message = "Lost connection";
		
		setChanged();
		notifyObservers();
	}
	
	/**
	 * The player disconnects from the client
	 */
	public void disconnect(){
		gameGrid.clearGrid();
		currentState = FINISHED;
		message = "Disconnected...";
		
		setChanged();
		notifyObservers();
		
		client.disconnect();
		
	}
	
	/**
	 * The player receives a move from the other player
	 * 
	 * @param x The x coordinate of the move
	 * @param y The y coordinate of the move
	 */
	public void receivedMove(int x, int y){
		
		gameGrid.move(x, y, gameGrid.OTHER);
		
		if (gameGrid.isWinner(gameGrid.OTHER)) {
			currentState = FINISHED;
			message = "You lost!";
		} else {
			currentState = MY_TURN;
			message = "Your turn";
		}
		
		setChanged();
		notifyObservers();
		
	}
	
	/**
	 * Updates the GUI	 
	 * 
	 * @param observable object
	 * @param object
	 */
	public void update(Observable o, Object arg) {
		
		switch(client.getConnectionStatus()){
		case GomokuClient.CLIENT:
			message = "Game started, it is your turn!";
			currentState = MY_TURN;
			break;
		case GomokuClient.SERVER:
			message = "Game started, waiting for other player...";
			currentState = OTHER_TURN;
			break;
		}
		setChanged();
		notifyObservers();
		
		
	}
	
}
