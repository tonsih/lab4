package lab4.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import lab4.client.GomokuClient;
import lab4.data.GameGrid;
import lab4.data.GomokuGameState;

/**
 * The GUI class
 * @author Toni Sihvola
 * @author Mojtaba Rezaei
 */

public class GomokuGUI implements Observer {

	private GomokuClient client;
	private GomokuGameState gamestate;
	private GamePanel gameGridPanel;
	private JLabel messageLabel;
	private JButton connectButton, disconnectButton, newGameButton;

	/**
	 * The constructor
	 * 
	 * @param g The game state that the GUI will visualize
	 * @param c The client that is responsible for the communication
	 */
	public GomokuGUI(GomokuGameState g, GomokuClient c) {
		this.client = c;
		this.gamestate = g;
		client.addObserver(this);
		gamestate.addObserver(this);

		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();

		SpringLayout layout = new SpringLayout();

		panel.setLayout(layout);
		panel.setSize(500, 500);

		gameGridPanel = new GamePanel(g.getGameGrid());
		gameGridPanel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
				int[] positions = gameGridPanel.getGridPosition(e.getX(), 
																e.getY());
				g.move(positions[0], positions[1]);
				gameGridPanel.repaint();

			}
		});

		connectButton = new JButton("Connect");
		connectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new ConnectionWindow(c);
			}

		});

		newGameButton = new JButton("New Game");
		newGameButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				g.newGame();
			}
		});

		disconnectButton = new JButton("Disconnect");
		disconnectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				g.disconnect();

			}

		});

		messageLabel = new JLabel("Welcome!");

		panel.add(gameGridPanel);
		panel.add(messageLabel);
		panel.add(connectButton);
		panel.add(disconnectButton);
		panel.add(newGameButton);

		layout.putConstraint(SpringLayout.WEST, gameGridPanel, 20, SpringLayout.WEST, panel);
		layout.putConstraint(SpringLayout.NORTH, gameGridPanel, 10, SpringLayout.NORTH, panel);
		layout.putConstraint(SpringLayout.WEST, connectButton, 20, SpringLayout.WEST, panel);
		layout.putConstraint(SpringLayout.WEST, messageLabel, 20, SpringLayout.WEST, panel);
		
		layout.putConstraint(SpringLayout.NORTH, connectButton, 10, SpringLayout.SOUTH, gameGridPanel);
		layout.putConstraint(SpringLayout.NORTH, newGameButton, 10, SpringLayout.SOUTH, gameGridPanel);
		layout.putConstraint(SpringLayout.NORTH, disconnectButton, 10, SpringLayout.SOUTH, gameGridPanel);
		layout.putConstraint(SpringLayout.NORTH, messageLabel, 10, SpringLayout.SOUTH, gameGridPanel);
		
		layout.putConstraint(SpringLayout.WEST, newGameButton, 10, SpringLayout.EAST, connectButton);
		layout.putConstraint(SpringLayout.WEST, disconnectButton, 10, SpringLayout.EAST, newGameButton);

		layout.putConstraint(SpringLayout.NORTH, messageLabel, 10, SpringLayout.SOUTH, connectButton);

		frame.setLocation(0, 0);
		frame.setPreferredSize(new Dimension(900, 900));
		frame.add(panel);
		frame.pack();
		frame.setVisible(true);

	}
	
	
	
	/**
	 * Updates the GUI
	 * 
	 * @param observable object
	 * @param object
	 */
	public void update(Observable arg0, Object arg1) {

		// Update the buttons if the connection status has changed
		if (arg0 == client) {
			if (client.getConnectionStatus() == GomokuClient.UNCONNECTED) {
				connectButton.setEnabled(true);
				newGameButton.setEnabled(false);
				disconnectButton.setEnabled(false);
			} else {
				connectButton.setEnabled(false);
				newGameButton.setEnabled(true);
				disconnectButton.setEnabled(true);
			}
		}

		// Update the status text if the gamestate has changed
		if (arg0 == gamestate) {
			messageLabel.setText(gamestate.getMessageString());
		}

	}

}
