package lab4;

import lab4.client.GomokuClient;
import lab4.data.GomokuGameState;
import lab4.gui.GomokuGUI;

/**
 * Main class of the program 
 * @author Toni Sihvola
 * @author Mojtaba Rezaei
 */

public class GomokuMain {
	private static int port;
	
	public static void main(String[] args) {
		
		try {
			port = Integer.parseInt(args[0]);
			if (port < 0 || args.length > 1) {
				throw new IllegalArgumentException();
			} 
			
			
		} catch (ArrayIndexOutOfBoundsException | IllegalArgumentException e) {
			port = 4010;
			System.out.println("Port is now: " + port);
		}
		
		
		System.out.println(port);
		
		GomokuClient gomokuClient = new GomokuClient(port);
		GomokuGameState gameState = new GomokuGameState(gomokuClient);
		new GomokuGUI(gameState, gomokuClient);
		
	}
}
